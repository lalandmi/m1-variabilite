#README

If you are a UGA student (ERCA included), first log onto the university notebook server:

http://jupyterhub.u-ga.fr

Open a new terminal : `New` > `Terminal`

we will use `git` to download the practical material into your work area. for this, you should copy - paste each of the following lines into the terminal and execute it

	cd ~/notebooks
	git clone https://gricad-gitlab.univ-grenoble-alpes.fr/voisind/m1-variabilite.git # clone the project
	cd m1-variabilite
	mkdir my-work
	cp -r DO-NOT-USE-original-notebooks/ERCA_intro-data_p1_data-munging.ipynb my-work

this downloaded into your `notebooks`folder (the one where jupyter lives) the practical material from my git repository (second line), then copied into a newly created (4th line) dedicated sub-folder (`my-work`) the notebooks we will play with



then close the tab containing the terminal

you should see a new `m1-variabilite` folder in your notebook list.

open it, and start working (ERCA_intro-data_p1_data-munging.ipynb) !

# session 2

I updated some clean data file in the `data` directory, as well as some details in the notebook for this session. 

here is what you should do to update your own jupyterhub:

first log onto the university notebook server:

http://jupyterhub.u-ga.fr

Open a new terminal : `New` > `Terminal`

if you followed my lead last week, then you should execute each of the following lines one by one (do not copy what is after #):

	cd ~/notebooks/m1-variabilite    # change directory ("cd") to this new folded
	git pull https://gricad-gitlab.univ-grenoble-alpes.fr/voisind/m1-variabilite.git # updates the project, normally without touching your work directory

this should just dowload whatever I modified, without touching anything else

from here, you close the tab containing the terminal

you should now have all that is needed for sesson 2!

open it, and start working (ERCA_intro-data_p2_groupby-resample.ipynb) !

